package org.mokolo.clipi;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.commons.io.FileUtils;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.property.RawProperty;
import lombok.extern.log4j.Log4j;

@Log4j
public class VCardDAO {
  private File vcardsDirectory;
  
  public VCardDAO(File vcardsDirectory) {
    this.vcardsDirectory = vcardsDirectory;
  }
  
  public VCardChain getChain(String id, String prefix, List<String> priorities)
  throws IOException, VCardNotFoundException {
    VCardChain chain = new VCardChain();
    VCard vcard = this.getVCard(id);
    chain.add(vcard);
    this.followChain(chain, vcard, prefix, priorities);
    return chain;
  }
  
  private void followChain(VCardChain chain, VCard vcard, String prefix, List<String> priorities)
  throws IOException, VCardNotFoundException {
    for (String priority : priorities) {
      String propertyName = prefix+priority;
      List<RawProperty> properties = vcard.getExtendedProperties(propertyName);
      if (! properties.isEmpty()) {
        String id = properties.get(0).getValue();
        VCard next = this.getVCard(id);
        chain.add(next);
        this.followChain(chain, next, prefix, priorities);
        break;
      }
    }
  }
  
  public List<String> getUids() {
    List<String> list = new ArrayList<String>();
    File files[] = this.vcardsDirectory.listFiles(new org.mokolo.commons.io.fp.RegexPatternFilter(".*\\.vcf"));
    for (File file : files) {
      String fileName = file.getName();
      list.add(fileName.substring(0, fileName.length()-4));
    }
    return list;
  }
  
  public VCard getVCard(String uid) throws VCardNotFoundException, IOException {
    File file = new File(this.vcardsDirectory, uid+".vcf");
    if (! file.exists())
      throw new VCardNotFoundException("Cannot find VCard with uid '"+uid+"'");
    VCard vcard = Ezvcard.parse(file).first();
    return vcard;
  }

  public void putVCard(VCard contact) throws IOException {
    File file = new File(this.vcardsDirectory, contact.getUid().getValue()+".vcf");
    if (! file.exists())
      log.info("Creating new file "+file.getAbsolutePath());
    else
      log.info("Overwriting existing file "+file.getAbsolutePath());
    FileUtils.writeStringToFile(file, Ezvcard.write(contact).go(), "UTF-8"); 
  }

}
