package org.mokolo.clipi;

import java.io.File;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils; 
import org.joda.time.DateTime;

import ezvcard.VCard;
import ezvcard.property.Birthday;
import ezvcard.property.FormattedName;
import lombok.extern.log4j.Log4j;

@Log4j
public class BirthdayCalendarGenerator {
  private File vcardsDirectory;

  private static String[] MONTHS = {
      "January", "February", "March", "April", "May", "June",
      "July", "August", "September", "October", "November", "December" };
  
  public BirthdayCalendarGenerator(File vcardsDirectory) {
    this.vcardsDirectory = vcardsDirectory;
  }

  public void printCalendar() {
    /* Outer map maps months to maps with days
     * Inner map maps days to list of names
     */
    Map<Integer, Map<Integer, List<String>>> calendar = new HashMap<Integer, Map<Integer, List<String>>>(); 

    try {
      VCardDAO loader = new VCardDAO(this.vcardsDirectory);
      List<String> uids = loader.getUids();
      for (String uid : uids) {
        try {
          
          VCard card = loader.getVCard(uid);
          Birthday bday = card.getBirthday();
          if (bday != null) {
            Date bdayDate = bday.getDate();
            if (bdayDate != null) {
              DateTime dt = new DateTime(bdayDate);
              FormattedName fn = card.getFormattedName();
              if (fn != null) {
                store(calendar, dt.getYear(), dt.getMonthOfYear(), dt.getDayOfMonth(), fn.getValue());
              }
            }
          }
        } catch (VCardNotFoundException e) {
          log.error("Card with uid '"+uid+"' not found. Possibly bad reference.");
        }
      }
      
      for (Integer month : getSortedMonthsList(calendar)) {
        System.out.println("-- "+StringUtils.rightPad(MONTHS[month-1]+" ", 32, '-'));
        System.out.println();
        for (Integer day : getSortedDaysList(calendar, month)) {
          List<String> names = getSortedNamesList(calendar, month, day);
          System.out.println(String.format("%02d %s", day, names.get(0)));
          for (int i=1; i<names.size(); i++)
            System.out.println(String.format("%2s %s", "", names.get(i)));
        }
        System.out.println();
      }
      
    } catch (IOException e) {
      e.printStackTrace();
    }
  }
  
  private static void store(Map<Integer, Map<Integer, List<String>>> calendar, int year, int month, int day, String fn) {
    Map<Integer, List<String>> days = calendar.get(month);
    if (days == null) {
      days = new HashMap<Integer, List<String>>();
      calendar.put(month, days);
    }
    List<String> names = days.get(day);
    if (names == null) {
      names = new ArrayList<String>();
      days.put(day,  names);
    }
    names.add(fn + " (" + year + ")");
  }
  
  private List<Integer> getSortedMonthsList(Map<Integer, Map<Integer, List<String>>> calendar) {
    List<Integer> list = new ArrayList<Integer>();
    for (Integer month : calendar.keySet())
      list.add(month);
    Collections.sort(list);
    return list;
  }

  private List<Integer> getSortedDaysList(Map<Integer, Map<Integer, List<String>>> calendar, Integer month) {
    List<Integer> list = new ArrayList<Integer>();
    Map<Integer, List<String>> days = calendar.get(month);
    for (Integer day : days.keySet())
      list.add(day);
    Collections.sort(list);
    return list;
  }

  private List<String> getSortedNamesList(Map<Integer, Map<Integer, List<String>>> calendar, Integer month, Integer day) {
    List<String> list = new ArrayList<String>();
    Map<Integer, List<String>> days = calendar.get(month);
    for (String name : days.get(day))
      list.add(name);
    Collections.sort(list);
    return list;
  }

}

