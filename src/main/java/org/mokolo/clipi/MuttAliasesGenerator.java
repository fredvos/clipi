package org.mokolo.clipi;

import java.io.File;

import java.io.IOException;
import java.util.Collections;
import java.util.List;

import ezvcard.property.Email;
import lombok.extern.log4j.Log4j;

@Log4j
public class MuttAliasesGenerator {
  private File vcardsDirectory;

  public MuttAliasesGenerator(File vcardsDirectory) {
    this.vcardsDirectory = vcardsDirectory;
  }

  public void printAliases(String prefix, List<String> priorities) {
    try {
      VCardDAO loader = new VCardDAO(this.vcardsDirectory);
      List<String> uids = loader.getUids();
      Collections.sort(uids);
      for (String uid : uids) {
        try {
          VCardChain chain = loader.getChain(uid, prefix, priorities);
          if (! chain.isGroupFirst()) {
            if (! chain.getEmails().isEmpty()) {
              String email = formatEmail(chain);
              System.out.println(String.format("alias %s\t%s", uid, email));
            }
          }
          else {
            List<String> members = chain.getMembersFirst();
            StringBuffer buf = new StringBuffer();
            for (String member : members) {
              try {
                VCardChain subchain = loader.getChain(member, prefix, priorities);
                if (! subchain.getEmails().isEmpty()) {
                  String email = formatEmail(subchain);
                  if (buf.length() > 0)
                    buf.append(",");
                  buf.append(email);
                }
              } catch (FieldNotFoundException e) {
                log.debug("Cannot find field for card with uid '"+uid+"'. Msg="+e.getMessage());
              }
            }
            System.out.println(String.format("alias %s\t%s", uid, buf.toString()));
          }
        } catch (VCardNotFoundException e) {
          log.error("Card with uid '"+uid+"' not found. Possibly bad reference.");
        } catch (EmptyChainException e) {
          log.error("Empty chain for card with uid '"+uid+"'");
        } catch (FieldNotFoundException e) {
          log.debug("Cannot find field for card with uid '"+uid+"'. Msg="+e.getMessage());
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  private String formatEmail(VCardChain chain)
      throws EmptyChainException, FieldNotFoundException {
    Email email = chain.getEmails().get(0);
    String formattedName = chain.getFormattedName().getValue();
    return String.format("%s <%s>", formattedName, email.getValue());
  }

}

