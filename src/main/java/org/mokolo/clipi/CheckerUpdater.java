package org.mokolo.clipi;

import java.io.File;
import java.io.IOException;
import java.util.UUID;

import ezvcard.VCard;
import ezvcard.property.Uid;

import lombok.extern.log4j.Log4j;

@Log4j
public class CheckerUpdater {
  private File vcardsDirectory;

  public CheckerUpdater(File vcardsDirectory) {
    this.vcardsDirectory = vcardsDirectory;
  }

  public void checkAndUpdate(int maxUpdates) {
    try {
      VCardDAO dao = new VCardDAO(this.vcardsDirectory);
      int updates = 0;
      for (String uid : dao.getUids()) {
        VCard contact = dao.getVCard(uid);
        if (contact.getUid() == null || contact.getUid().getValue() == null) {
          log.warn("UID of "+uid+".vcf missing");
        }
        else if (! isUUID(contact.getUid().getValue())) {
          log.warn("UID of "+uid+".vcf is not a UUID");
          String uidFromVcf = contact.getUid().getValue();     
          if (uidFromVcf.length() == 45) {
            String uuidPart = uidFromVcf.substring(9);
            try {
              UUID uuid = UUID.fromString(uuidPart);
              log.info("Updating UID of "+uid+".vcf");
              contact.setUid(new Uid(uuid.toString()));
              
              dao.putVCard(contact);
              updates++;
            } catch (IllegalArgumentException e) { }
          }
        }
        else if (! contact.getUid().getValue().equals(uid)) {
          log.warn("UID of "+uid+".vcf != basename file");
        }
        if (updates >= maxUpdates) {
          log.info("Max updates reached");
          break;
        }
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (VCardNotFoundException e) {
      e.printStackTrace();
    }
  }
  
  private static boolean isUUID(String s) {
    try {
      UUID.fromString(s);
      return true;
    } catch (IllegalArgumentException e) {
      return false;
    }
  }
}

