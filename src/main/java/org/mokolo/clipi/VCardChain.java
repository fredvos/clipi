package org.mokolo.clipi;

import java.util.ArrayList;
import java.util.List;

import ezvcard.VCard;
import ezvcard.property.Address;
import ezvcard.property.Birthday;
import ezvcard.property.Email;
import ezvcard.property.FormattedName;
import ezvcard.property.Kind;
import ezvcard.property.Member;
import ezvcard.property.StructuredName;
import ezvcard.property.Telephone;

public class VCardChain extends ArrayList<VCard> {
  private static final long serialVersionUID = 1535605638326069787L;

  public VCardChain() {
    super();
  }
  
  public List<Address> getAddresses() throws EmptyChainException, FieldNotFoundException {
    this.testChain();
    List<Address> list = new ArrayList<>();
    for (VCard vcard : this)
      for (Address address : vcard.getAddresses())
        list.add(address);
    return list;
  }

  public boolean isGroupFirst() throws EmptyChainException {
    this.testChain();
    VCard vcard = this.get(0);
    Kind kind = vcard.getKind();
    if (kind != null && kind.getValue().equals("group"))
      return true;
    else
      return false;
  }
  
  public List<String> getMembersFirst() throws EmptyChainException {
    this.testChain();
    List<String> list = new ArrayList<String>();
    VCard vcard = this.get(0);
    for (Member member : vcard.getMembers())
      list.add(member.getValue());
    return list;
  }
  
  public FormattedName getFormattedName() throws EmptyChainException, FieldNotFoundException {
    this.testChain();
    FormattedName formattedName = null;
    for (VCard vcard : this) {
      formattedName = vcard.getFormattedName();
      if (formattedName != null)
        return formattedName;
    }
    throw new FieldNotFoundException("Cannot find formatted name in chain");
  }
  
  public StructuredName getStructuredName() throws EmptyChainException, FieldNotFoundException {
    this.testChain();
    StructuredName structuredName = null;
    for (VCard vcard : this) {
      structuredName = vcard.getStructuredName();
      if (structuredName != null)
        return structuredName;
    }
    throw new FieldNotFoundException("Cannot find structured name in chain");
  }
  
  public List<Email> getEmails() throws EmptyChainException {
    this.testChain();
    List<Email> list = new ArrayList<>();
    for (VCard vcard : this)
      for (Email email : vcard.getEmails())
        list.add(email);
    return list;
  }
  
  public List<Telephone> getPhoneNumbers() throws EmptyChainException {
    this.testChain();
    List<Telephone> list = new ArrayList<>();
    for (VCard vcard : this)
      for (Telephone telephone : vcard.getTelephoneNumbers())
        list.add(telephone);
    return list;
  }
  
  public Birthday getBirthday() throws EmptyChainException, FieldNotFoundException {
    this.testChain();
    Birthday birthday = null;
    for (VCard vcard : this) {
      birthday = vcard.getBirthday();
      if (birthday != null)
        return birthday;
    }
    throw new FieldNotFoundException("Cannot find structured name in chain");
  }
  
  private void testChain() throws EmptyChainException {
    if (this.isEmpty())
      throw new EmptyChainException();
  }
}
