package org.mokolo.clipi;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import joptsimple.OptionParser;
import joptsimple.OptionSet;
import lombok.extern.log4j.Log4j;

import org.mokolo.commons.cli.CommandDefinition;
import org.mokolo.commons.cli.CommandParser;
import org.mokolo.commons.io.conf.Configuration;
import org.mokolo.commons.io.conf.ConfigurationLoader;
import org.mokolo.commons.io.log4j.Log4JConfiguration;
import org.mokolo.commons.lang.document.Document;

@Log4j
public class CLI {

  private CLI() {}

  public static void main(String[] args) {

    try {
      CommandParser commandParser = new CommandParser(new OptionParser("v"), "request");
      commandParser.addCommandDefinition(new CommandDefinition("assemble-contacts", null)
          .addRequiredArgument("group"));
      commandParser.addCommandDefinition(new CommandDefinition("check-update", null));
      commandParser.addCommandDefinition(new CommandDefinition("print-birthdaycalendar", null));
      commandParser.addCommandDefinition(new CommandDefinition("print-muttaliases", null));
      OptionSet options = commandParser.getOptionParser().parse(args);

      Log4JConfiguration log4j = new Log4JConfiguration();
      Log4JConfiguration.Level level = Log4JConfiguration.Level.NORMAL;
      if (options.has("v"))
        level = Log4JConfiguration.Level.VERBOSE;
      log4j.addConsoleAppender(level, Log4JConfiguration.NONE);
      log4j.addLoggers(level, "org.mokolo.clipi", "org.mokolo.commons");

      Configuration configuration = ConfigurationLoader.getConfiguration("clipi");
      
      File baseDirectory = configuration.getFile("clipi.data-directory");
      File vcardsDirectory = new File(baseDirectory, "names");

      String prefix = parseLinksPrefix(configuration);
      List<String> priorities = parseLinksPriorities(configuration);
      
      Document document = commandParser.parseCommandWithSwitches(options);
      commandParser.validateDocument(document);
      if (document.getString("request").equals("assemble-contacts")) {
        String group = document.getString("group");
        AssemblerGenerator generator = new AssemblerGenerator(vcardsDirectory, group);
        generator.printContacts(prefix, priorities);
      } 
      else if (document.getString("request").equals("check-update")) {
        CheckerUpdater generator = new CheckerUpdater(vcardsDirectory);
        generator.checkAndUpdate(1);
      } 
      else if (document.getString("request").equals("print-birthdaycalendar")) {
        BirthdayCalendarGenerator generator = new BirthdayCalendarGenerator(vcardsDirectory);
        generator.printCalendar();
      }
      else if (document.getString("request").equals("print-muttaliases")) {
        MuttAliasesGenerator generator = new MuttAliasesGenerator(vcardsDirectory);
        generator.printAliases(prefix, priorities);
      } 
    } catch (Exception e) {
      log.error("Client exception: " + e.toString());
      e.printStackTrace();
    }
  }
  
  private static String parseLinksPrefix(Configuration configuration) {
    return configuration.getString("clipi.names.links-prefix", "X-MOKOLO-");
  }
  
  private static List<String> parseLinksPriorities(Configuration configuration) {
    List<String> list = new ArrayList<String>();
    String priorities = configuration.getString("clipi.names.links-priorities", "IS,HOME,WORK,SCHOOL,ORG");
    String[] splitted = priorities.split(",");
    for (String priority : splitted)
      list.add(priority);
    return list;
  }
}
