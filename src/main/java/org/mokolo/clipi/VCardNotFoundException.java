package org.mokolo.clipi;

public class VCardNotFoundException extends Exception {
  private static final long serialVersionUID = -8169592713546237335L;

  public VCardNotFoundException(String msg) {
    super(msg);
  }
}
