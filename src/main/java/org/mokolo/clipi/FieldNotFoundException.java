package org.mokolo.clipi;

public class FieldNotFoundException extends Exception {
  private static final long serialVersionUID = -7940877413778926893L;

  public FieldNotFoundException(String msg) {
    super(msg);
  }

}
