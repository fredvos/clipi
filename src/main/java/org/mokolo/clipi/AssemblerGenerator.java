package org.mokolo.clipi;

import java.io.File;

import java.io.IOException;
import java.util.List;

import ezvcard.Ezvcard;
import ezvcard.VCard;
import ezvcard.VCardVersion;
import ezvcard.property.Address;
import ezvcard.property.Email;
import ezvcard.property.Kind;
import ezvcard.property.Member;
import ezvcard.property.Telephone;

public class AssemblerGenerator {
  private File vcardsDirectory;
  private String group;

  public AssemblerGenerator(File vcardsDirectory, String group) {
    this.vcardsDirectory = vcardsDirectory;
    this.group = group;
  }

  public void printContacts(String prefix, List<String> priorities) {
    try {
      VCardDAO dao = new VCardDAO(this.vcardsDirectory);
      VCard contactsGroup = dao.getVCard(group);
      for (Member member : contactsGroup.getMembers()) {
        VCard contact = new VCard();
        contact.setVersion(VCardVersion.V4_0);
        contact.setKind(Kind.individual());
        VCardChain chain = dao.getChain(member.getValue(), prefix, priorities);
        contact.setNickname(chain.get(0).getNickname());
        contact.setUid(chain.get(0).getUid());
        contact.setFormattedName(chain.getFormattedName());
        try {
          contact.setStructuredName(chain.getStructuredName());
        } catch (FieldNotFoundException e) {
          ; // Ok
        }
        for (Address address : chain.getAddresses())
          contact.addAddress(address);
        for (Email email : chain.getEmails())
          contact.addEmail(email);
        for (Telephone telephone : chain.getPhoneNumbers())
          contact.addTelephoneNumber(telephone);
        try {
          contact.setBirthday(chain.getBirthday());
        } catch (FieldNotFoundException e) {
          ; // Ok
        }
        System.out.println(Ezvcard.write(contact).go());
      }
    } catch (IOException e) {
      e.printStackTrace();
    } catch (VCardNotFoundException e) {
      e.printStackTrace();
    } catch (EmptyChainException e) {
      e.printStackTrace();
    } catch (FieldNotFoundException e) {
      e.printStackTrace();
    }
  }
}

