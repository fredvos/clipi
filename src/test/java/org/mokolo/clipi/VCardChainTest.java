package org.mokolo.clipi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import ezvcard.Ezvcard;
import ezvcard.VCard;

public class VCardChainTest {

  @Test
  public void canFollowChain() throws Exception {
    VCardChain chain = this.setupChain();
    assertEquals("Geert van der Test", chain.getFormattedName().getValue(), "Full name matches");
    assertEquals("geert.vandertest@gmail.com", chain.getEmails().get(0).getValue(), "Email address matches");
    assertEquals("Ontmoetingsstraat 18", chain.getAddresses().get(0).getStreetAddress(), "Street address matches");
  }
  
  private VCardChain setupChain() throws IOException {
    VCardChain chain = new VCardChain();
    InputStream is = null;
    
    is = this.getClass().getResourceAsStream("/names/geert.vcf");
    VCard son = Ezvcard.parse(is).first();
    is.close();
    chain.add(son);

    is = this.getClass().getResourceAsStream("/names/vandertest.vcf");
    VCard family = Ezvcard.parse(is).first();
    is.close();
    chain.add(family);

    return chain;
  }
}

