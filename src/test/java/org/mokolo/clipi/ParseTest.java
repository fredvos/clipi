package org.mokolo.clipi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.IOException;
import java.io.InputStream;

import org.junit.jupiter.api.Test;

import ezvcard.Ezvcard;
import ezvcard.VCard;

public class ParseTest {

  @Test
  public void canParseCard() throws IOException {
    InputStream is = this.getClass().getResourceAsStream("/names/vandertest.vcf");
    VCard vcard = Ezvcard.parse(is).first();
    is.close();
    assertEquals("Familie van der Test", vcard.getFormattedName().getValue(), "Full name matches");
    assertEquals("Ontmoetingsstraat 18", vcard.getAddresses().get(0).getStreetAddress(), "Street address matches");
  }
}
