package org.mokolo.clipi;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import ezvcard.VCard;

public class VCardDAOTest {

  private static String PREFIX = "X-MOKOLO-";
  
  @Test
  public void canLoadCard() throws Exception {
    VCardDAO dao = generateVCardDAO();
    VCardChain chain = dao.getChain("vandertest", PREFIX, this.buildPriorities());
    
    assertEquals("Familie van der Test", chain.getFormattedName().getValue(), "Full name matches");
    assertEquals("Ontmoetingsstraat 18", chain.getAddresses().get(0).getStreetAddress(), "Street address matches");
  }

  @Test
  public void canLoadChain() throws Exception {
    VCardDAO dao = generateVCardDAO();
    VCardChain chain = dao.getChain("geert", PREFIX, this.buildPriorities());
    
    assertEquals(2, chain.size(), "Two entries");
    VCard son = chain.get(0);
    VCard family = chain.get(1);
    assertEquals("Geert van der Test", son.getFormattedName().getValue(), "Son full name matches");
    assertEquals("Familie van der Test", family.getFormattedName().getValue(), "Family full name matches");
  }

  private List<String> buildPriorities() {
    List<String> priorities = new ArrayList<String>();
    priorities.add("IS");
    priorities.add("HOME");
    return priorities;
  }
  
  private static VCardDAO generateVCardDAO() {
    File namesDirectory = new File("src/test/resources/names".replaceAll("/", File.separator));
    return  new VCardDAO(namesDirectory);
  }

}
