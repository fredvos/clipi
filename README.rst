=====
CLIPI
=====

Command Line Interface Personal Information
___________________________________________

Application to handle personal information stuff.
Currently it only is an address book that uses a directory with vCard 4.0 files to store names, addresses, phone numbers et cetera.

Rules for names
+++++++++++++++

- Store the vCard files in a directory called 'names'
- One .vcf file per person, address or family
- Per card one address or phone number of each type (work, home, mobile); use extra cards if necessary
- If a card has an address or phone number of some kind, this will be used
- If a card does not have an address or phone number of some kind,
  but has a relation field, the data in that related card will be used
- Do not repeat anything; use relations instead

Relations between vCards
++++++++++++++++++++++++

With X-MOKOLO-\* fields you can connect cards.
Examples:

- X-MOKOLO-IS points to same person
- X-MOKOLO-HOME points to the home address
- X-MOKOLO-ORG points to organisation address
- X-MOKOLO-WORK points to the work address
- X-MOKOLO-SCHOOL points to school adress

The 'X-MOKOLO-' prefix is configurable, but 'X-MOKOLO-' is the default.
You can set your own prefix in the configuration.
Make sure it starts with 'X-'.

The 'IS', 'HOME', 'ORG', 'WORK' and 'SCHOOL' suffixes are examples.
These are used in the default configuration.
You are free to choose other strings.

If multiple relation pointers of different types available,
Names will take the first relation available, using a priority order from configuration. 

Example situations:

A family with three children, all living at same address.
There's a home phone, but all family members have a mobile phone too.
The family has an email address, but some children have their own email address.

- One card for the family, with home address, home phone and shared email address
- One card for each family member, with mobile phone address, birthday en email address
- Cards for family member have a X-MOKOLO-HOME:uid field pointing to the family card

A person living on his own, with several personal email addresses.

- One card for the person, with home address, home phone, cell phone and one private email address
- One card for each alternative email address, with X-MOKOLO-IS:uid pointing to base card OR
  A card for an organisation with X-MOKOLO-HOME pointing to base card AND a card for the alternative persona
  with email address and X-MOKOLO-* pointing to this organisation card

Build Clipi
+++++++++++

- Make sure you forked my commons repository (https://github.com/fredvos/commons)
  and installed all its components to your local Maven repository
- Fork the Clipi repository
- CD to the base directory, where pom.xml is located
- Build jar: ``$ mvn package`` (results in target/clipi-0.1-SNAPSHOT-jar-with-dependencies.jar)

Test data
+++++++++
The test/resources directory contains a names directory with four vCard files that are used for tests.
Things may have changed during futher development.
Extra cards may have been added.

File **friends.vcf** contains a list of cards.
It had no email address.
Therefore Names will try to list all email addresses of its members.::

  BEGIN:VCARD
  FN:My friends
  KIND:group
  MEMBER:geert
  MEMBER:peter
  VERSION:4.0
  END:VCARD

File **geert.vcf** is the card for a member of the 'Van der Test' family.
He has his own email address, but no ADR field.
Due to the ``X-MOKOLO-HOME:vandertest`` line, the address in the 'vandertest.vcf' file is his address.::

  BEGIN:VCARD
  BDAY:1998-05-01
  EMAIL:geert.vandertest@gmail.com
  FN:Geert van der Test
  N:Test;Geert van der;;;
  VERSION:4.0
  X-MOKOLO-HOME:vandertest
  END:VCARD
  
File **peter.vcf** is for a friend called Peter Friend with the N field completely filled.
He has his own address and email address.::

  BEGIN:VCARD
  ADR:;;Laan van Vriendschap 22;Oudestad;;1854 XY;Netherlands
  BDAY:1963-08-18
  EMAIL:peter.friend@gmail.com
  FN:Peter Friend
  N:Friend;Peter;Johannes,Maria;dr.,ir.;MSc
  VERSION:4.0
  END:VCARD

File **vandertest.vcf** is for a family.
Their home address is in the card.::

  BEGIN:VCARD
  ADR:;;Ontmoetingsstraat 18;Oss;;5340 AB;Netherlands
  FN:Familie van der Test
  VERSION:4.0
  END:VCARD

Test run
++++++++
The test run uses the clipi.conf file in the base dir of Clipi, where pom.xml is located.
This config file sets the clipi.data-directory to the test/resources directory,
so the test-data is used.

Configure
+++++++++

- Setup configuration file.
  Clipi checks the following locations for configuration files,
  loads them all in that order if available and replaces settings if overridden:

  + /etc/clipi.conf
  + $HOME/.clipi/clipi.conf
  + File pointed to by -Dclipi.conf setting in java command
  + File clipi.conf in current working directory

Use
+++

- Create a 'names' directory within clipi.data-directory (setting in configuration) with vCard files (use files in test/resources as samples)
- Use an editor to edit files (syntax of vCard files is quite simple)
- ``$ java -cp target/clipi-0.1-SNAPSHOT-jar-with-dependencies.jar org.mokolo.clipi.CLI <command> <arguments>``

Commands
++++++++

print-birthdaycalendar
  Prints simple birthday calendar to screen.
  Opens all vCard files in the names directory and prints a calendar.

print-muttaliases
  Prints Mutt aliases file to screen.
  Opens all .vcf files and tries to print a single line for the card if possible.
  If it can find an email address in the card or in another card in the chain, it will print the id of the card (using file name), followed by a TAB and the email address.
  The email address will be presented as 'FN <email>' (example: ``vosfmo Fred Vos <fred.vos@mokolo.org>``).
  If the .vcf file has KIND:group and no email address, it will generate a list of email addresses for all MEMBERs.

Configuration
+++++++++++++

clipi.data-directory
  Directory containing the names directory with vCard files

clipi.names.links-prefix
  Prefix for links.
  Default is 'X-MOKOLO-'.
  Always use a prefix that starts with 'X-'.
  
clipi.names.links-priorities
  Set priorities for following links like X-MOKOLO-\* or with another prefix (see names.links-prefix).
  Comma-separated list with for instance IS, HOME, WORK, SCHOOL, ORG, not containing spaces!
  You can use your own set of link types.
  If you leave SCHOOL out, for instance, then an X-MOKOLO-SCHOOL link will never be followed.
  Default is 'IS,HOME,WORK,SCHOOL,ORG'.
  At home you may prefer the default.
  At work you may prefer 'IS,WORK,SCHOOL,HOME,ORG'.

